﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace knn1
{
    public partial class Form1 : Form
    {
        public class KNN_TYPE
        {
            public  string[] KNN_DATA { set; get; }
            public  double   KNN_UZAKLIK { set; get; }
            public  string   KNN_K_SONUC { get; set; }

        }
        List<string[]> KNN_AYRILMIS = new List<string[]>();
        List<KNN_TYPE> KNN_AYRILMIS2 = new List<KNN_TYPE>();     
        double[] KNN_AYRILMIS_DEGER;
        string[] KNN_HESAPLANMAK_ISTENEN;   
        public int k_degeri;



        public Form1()
        {
            InitializeComponent();
        }

        private void button_yukle_h_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string file_adres = openFileDialog1.FileName;
            textBox_dosya_h.Text = file_adres;
            openFileDialog1.Dispose();

            if (file_adres != "")
            {
                string[] KNN_DATA_HAZIR = System.IO.File.ReadAllLines(file_adres);

                foreach (var item in KNN_DATA_HAZIR)
                {
                    string[] datalar = item.Split(',');
                 
                    KNN_AYRILMIS.Add(datalar);

                    foreach (var item2 in datalar)
                    {
                        richTextBox1.Text += item2;
                    }
                    richTextBox1.Text += "\n";
                    KNN_AYRILMIS2.Add(new KNN_TYPE{KNN_DATA = datalar, KNN_UZAKLIK = 0, KNN_K_SONUC = datalar[datalar.Count()-1]});
                    
                }
            }

            label_maxk.Text = KNN_AYRILMIS2.Count().ToString();

            KNN_AYRILMIS_DEGER = new double[KNN_AYRILMIS.Count];
        }

        private void button_yukle_i_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string file_adres = openFileDialog1.FileName;
            

            string[] KNN_DATA_ISTENEN = System.IO.File.ReadAllLines(file_adres);


            foreach (var item in KNN_DATA_ISTENEN)
            {
                textBox_dosya_i.Text += item;
            }
            KNN_HESAPLANMAK_ISTENEN = KNN_DATA_ISTENEN[0].Split(',');
            Console.Write("asd");

            try
            {
                richTextBox3.Clear();
                richTextBox2.Clear();

                if (k_degeri > KNN_AYRILMIS.Count || k_degeri < 0)
                {
                    MessageBox.Show("Sayı aralığını doğru girin");
                }
                else
                {

                    for (int i = 0; i < KNN_AYRILMIS.Count; i++)
                    {
                        double sayac1 = 0;

                        for (int k = 0; k < KNN_AYRILMIS[0].Count() - 1; k++)
                        {
                            int us_alma = Convert.ToInt32(KNN_HESAPLANMAK_ISTENEN[k]) - Convert.ToInt32(KNN_AYRILMIS[i][k]);
                            sayac1 += Math.Pow(us_alma, 2);
                        }

                        KNN_AYRILMIS_DEGER[i] = Math.Sqrt(sayac1);
                        KNN_AYRILMIS2[i].KNN_UZAKLIK = Math.Sqrt(sayac1);
                        richTextBox2.Text += Math.Sqrt(sayac1).ToString() + Environment.NewLine;

                    }

                    KNN_AYRILMIS2 = KNN_AYRILMIS2.OrderBy(a => a.KNN_UZAKLIK).ToList();


                    foreach (var item in KNN_AYRILMIS2.Select(x => x.KNN_DATA).ToArray())

                    {
                        foreach (var item2 in item)
                        {
                            richTextBox3.Text += item2;
                        }
                        richTextBox3.Text += Environment.NewLine;
                    }
                }

            }

            catch (Exception )
            {
                MessageBox.Show("Veriler Boş Girilemez");
            }


        }

        string most(string[] data)
        {
            int count = 1, tempCount;
            string frequentNumber = data[0];
            string tempNumber = "";
            for (int i = 0; i < (data.Length - 1); i++)
            {
                tempNumber = data[i];
                tempCount = 0;
                for (int j = 0; j < data.Length; j++)
                {
                    if (tempNumber == data[j])
                    {
                        tempCount++;
                    }
                }
                if (tempCount > count)
                {
                    frequentNumber = tempNumber;
                    count = tempCount;
                }
            }

            return frequentNumber;
        }

        private void buton_sonuc_Click(object sender, EventArgs e)
        {
            k_degeri = Convert.ToInt32(textBox_kdegeri.Text);

            var KNN_K_DEGERLI = KNN_AYRILMIS2.Take(k_degeri).ToList();


            for (int i = 0; i < KNN_K_DEGERLI.Select(x => x.KNN_UZAKLIK).ToArray().Count(); i++)
            {
                KNN_K_DEGERLI[i].KNN_UZAKLIK = (1 / Math.Pow(KNN_K_DEGERLI[i].KNN_UZAKLIK, 2));
            }

            double a = KNN_K_DEGERLI.Select(x => x.KNN_UZAKLIK).Max();
            int b = Array.IndexOf(KNN_K_DEGERLI.Select(x => x.KNN_UZAKLIK).ToArray(), a);


            //string ac = most(KNN_K_DEGERLI.Select(x => x.KNN_K_SONUC).ToArray());
            string ac = KNN_K_DEGERLI[b].KNN_K_SONUC;
            label_sonuc.Text = ac;



        }

        private void buton_grafik_Click(object sender, EventArgs e)
        {
            //if()
        }
    }
}
